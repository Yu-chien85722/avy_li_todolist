import React, { useEffect, useState, useCallback } from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import moment from "moment/moment";
import "react-big-calendar/lib/css/react-big-calendar.css";
import { parseBig5CSVFile } from "../utils";
import csvFilePath from "../ref_data/holiday.csv";
import cx from "classnames";
import {
  BsFillCircleFill,
  BsCheck2,
  BsChevronLeft,
  BsChevronRight,
} from "react-icons/bs";

const localizer = momentLocalizer(moment);

const MyCalendar = () => {
  const today = moment().startOf("day");
  const [events, setEvents] = useState();
  const [memos, setMemos] = useState([]);
  const [checks, setChecks] = useState([]);
  const [currentMonth, setCurrentMonth] = useState(moment().format("YYYY-MM"));

  useEffect(() => {
    parseBig5CSVFile(csvFilePath, (events) => {
      setEvents(events);
    });
  }, []);

  useEffect(() => {
    getMemosList();
    getChecksList();
  }, [currentMonth]);

  const eventStyleGetter = (event) => {
    const style = {
      backgroundColor: event.resource === "holiday" ? "red" : "blue",
    };
    return {
      style,
    };
  };

  const handleSelectSlot = (slotInfo) => {
    alert(slotInfo.start);
    console.log(slotInfo);
  };

  const CustomDateCell = ({ date, label }) => {
    // console.log(date);
    // console.log(today);

    const isToday = moment(date).startOf("day").isSame(today);
    const isHoliday = events?.some((event) =>
      moment(event.start).isSame(date, "day")
    );
    const hasMemo = memos?.some((memo) =>
      moment(memo.date).isSame(date, "day")
    );
    const hasCheck = checks?.some((check) =>
      moment(check.date).isSame(date, "day")
    );

    return (
      <div className="custom-date-cell">
        <div className={cx({ today: isToday }, { holiday: isHoliday })}>
          {label.charAt(0) === "0" ? label.slice(1) : label}
        </div>
        {hasCheck && <BsCheck2 color="#9EBF58" fontSize="1.5rem" />}
        {hasMemo && <BsFillCircleFill color="#FBB938" fontSize="0.5rem" />}
      </div>
    );
  };

  const CustomToolbar = ({ label, onNavigate }) => {
    return (
      <div className="custom-toolbar">
        <div>
          <button className="toolbar-button" onClick={() => onNavigate("PREV")}>
            <BsChevronLeft fontSize="1.5rem" color="#FBB938" />
          </button>
        </div>
        <div style={{ width: "8rem" }}>
          <span>{label}</span>
        </div>
        <div>
          <button className="toolbar-button" onClick={() => onNavigate("NEXT")}>
            <BsChevronRight fontSize="1.5rem" color="#FBB938" />
          </button>
        </div>
        <div>
          <button
            className="toolbar-button"
            onClick={() => onNavigate("TODAY")}
          >
            TODAY
          </button>
        </div>
      </div>
    );
  };

  const getMemosList = () => {
    fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
      .then((response) => response.json())
      .then((result) => {
        // console.log(result);
        // console.log(+currentMonth.slice(5));
        // console.log(generateRadomMark(+currentMonth.slice(5)));
        setMemos(generateRadomMark(+currentMonth.slice(5)));
      })
      .catch((error) => {
        console.lof(error);
      });
  };

  const getChecksList = () => {
    fetch("https://jsonplaceholder.typicode.com/posts/1/comments")
      .then((response) => response.json())
      .then((result) => {
        // console.log(result);
        // console.log(+currentMonth.slice(5));
        // console.log(generateRadomMark(+currentMonth.slice(5)));
        setChecks(generateRadomMark(+currentMonth.slice(5)));
      })
      .catch((error) => {
        console.lof(error);
      });
  };

  // when current date changed, this callback func will be executed
  // if current date is 7/4(new Date() obj), after user click next month, the current date will changed to 8/4 (new Date() obj) => execute func
  // newDate = current date
  const handleNavigate = useCallback(
    (newDate) => {
      // string
      const newMonth = moment(newDate).format("YYYY-MM");
      // update current month
      if (newMonth !== currentMonth) {
        setCurrentMonth(newMonth);
      }
    },
    [currentMonth]
  );

  return (
    <div>
      <Calendar
        localizer={localizer}
        // events={events}
        startAccessor="start"
        endAccessor="end"
        views={["month"]}
        style={{ height: "90vh" }}
        selectable="ignoreEvents"
        eventPropGetter={eventStyleGetter}
        onSelectSlot={handleSelectSlot}
        onNavigate={handleNavigate}
        components={{
          toolbar: CustomToolbar,
          month: {
            dateHeader: CustomDateCell, // Use custom date cell component
          },
        }}
      />
    </div>
  );
};

export default MyCalendar;

// dummy data
// 產生指定範圍的隨機數字
const generateRandomNumber = (min, max) => {
  const randomNumber = Math.floor(Math.random() * (max - min + 1)) + min;
  return randomNumber.toString();
};

// 取得隨機的日期
const generateRandomDate = (specMonth) => {
  const year = generateRandomNumber(2023, 2023);
  const month = generateRandomNumber(specMonth, specMonth);
  const day = generateRandomNumber(1, 31);
  // const hours = generateRandomNumber(0, 23);
  // const minutes = generateRandomNumber(0, 59);
  // const seconds = generateRandomNumber(0, 59);

  // return `${year}-${month.toString().padStart(2, '0')}-${day.toString().padStart(2, '0')} ${hours.toString().padStart(2, '0')}:${minutes
  // 	.toString()
  // 	.padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  return `${year}-${month.toString().padStart(2, "0")}-${day
    .toString()
    .padStart(2, "0")}`;
};

const generateRadomMark = (month) => {
  const marks = [];
  for (let i = 0; i < 3; i++) {
    const mark = {
      date: generateRandomDate(month),
      memo: "memo",
    };
    marks.push(mark);
  }
  return marks;
};
