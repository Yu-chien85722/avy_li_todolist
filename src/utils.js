import { fireEvent } from "@testing-library/react";
import Papa from "papaparse";

function parseBig5CSVFile(filePath, callback) {
  const reader = new FileReader();
  // 讀取 CSV 檔案
  fetch(filePath)
    .then((response) => response.blob())
    .then((blob) => {
      reader.readAsArrayBuffer(blob);
    })
    .catch((error) => {
      console.error("Error reading CSV file:", error);
    });

  reader.onload = function (event) {
    const fileData = event.target.result;

    // 使用 TextDecoder 解碼 Big5 編碼的資料
    const decoder = new TextDecoder("big5");
    const decodedData = decoder.decode(fileData);

    // 使用 Papaparse 解析解碼後的 CSV 字串
    Papa.parse(decodedData, {
      header: true,
      complete: function (results) {
        const rows = results.data;
        // console.log(rows);
        // Process the events array as needed
        const events = [];
        for (let i = 1; i < rows.length; i++) {
          if (
            !rows[i].Subject &&
            !rows[i]["Start Date"] &&
            !rows[i]["End Date"]
          ) {
            continue;
          }
          const startDateArr = rows[i]["Start Date"].split("/");
          const endDateArr = rows[i]["End Date"].split("/");
          const event = {
            title: rows[i].Subject,
            start: new Date(
              startDateArr[0],
              +startDateArr[1] - 1,
              startDateArr[2]
            ),
            end: new Date(endDateArr[0], +endDateArr[1] - 1, endDateArr[2]),
            allDay: rows[i]["All Day Event"] === "TRUE",
            resource:
              rows[i].Subject.indexOf("上班") === -1 ? "holiday" : "workday",
          };

          events.push(event);
        }

        callback(events);
      },
      error: function (error) {
        console.log("CSV parsing error:", error);
      },
    });
  };
}

export { parseBig5CSVFile };
