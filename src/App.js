import "./App.scss";
import MyCalendar from "./component/MyCalendar";

function App() {
  return (
    <div className="App">
      {/* <div>
        <iframe
          src="https://calendar.google.com/calendar/embed?height=600&wkst=1&bgcolor=%23ffffff&ctz=Asia%2FTaipei&showTitle=0&showNav=1&showDate=1&showPrint=0&showTabs=0&showCalendars=0&showTz=0&mode=MONTH&src=emgtdHcudGFpd2FuI2hvbGlkYXlAZ3JvdXAudi5jYWxlbmRhci5nb29nbGUuY29t&color=%230B8043"
          style={{ borderWidth: 0 }}
          width="800"
          height="600"
          frameborder="0"
        ></iframe>
      </div> */}
      <MyCalendar />
    </div>
  );
}

export default App;
